import {Container, styled} from "@mui/material";

export const LayoutContainer = styled(Container)({
    paddingTop: "50px",
    "@media (max-width: 500px)": {
        paddingTop: "25px"
    }
})